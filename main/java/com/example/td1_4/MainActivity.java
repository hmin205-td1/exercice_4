package com.example.td1_4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView=(TextView) findViewById(R.id.text);

        String string = getString(R.string.message1);
        String string1 = getString(R.string.message2);
        String string2 = getString(R.string.message3);

        button = (Button) findViewById(R.id.mainbutton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(),string, Toast.LENGTH_LONG).show();
            }
        });
        button1 = (Button) findViewById(R.id.mainbutton1);
        button1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                Toast.makeText(getApplicationContext(),string1, Toast.LENGTH_LONG).show();
                textView.setText(string2);
                return true;
            }
        });

    }
}